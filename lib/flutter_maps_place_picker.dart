library flutter_maps_place_picker;

export 'src/models/pick_result.dart';
export 'src/components/floating_card.dart';
export 'src/components/rounded_frame.dart';
export 'src/place_picker.dart';

/**
 * searching nearby places, not only when pin is exactly in road
 */
  